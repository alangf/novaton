import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import Home from '@/components/Home'
import Play from '@/components/Play'
import Ganaste from '@/components/Ganaste'
import Cuadros from '@/components/Cuadros'
import Ranking from '@/components/Ranking'
import Perdiste from '@/components/Perdiste'
import axios from 'axios'
import Words from '../Words'

Vue.use(Router)

const router = new Router({
    base: '/campanas/novaton-2017/',
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
    {
      path: '/play',
      name: 'play',
      component: Play,
      beforeEnter (to, from, next) {
        if (!store.state.isLoggedIn)
          next('/')
        else
          next()
      }
  	},
  	{
      path: '/ganaste',
      name: 'ganaste',
      component: Ganaste,
      beforeEnter: (to, from, next) => {
        if (!store.state.isLoggedIn || from.name != 'play' && !store.state.debug)
          next('/')
        else
          next()
      }
    },
    {
      path: '/cuadros',
      name: 'cuadros',
      component: Cuadros
    },
    {
      path: '/ranking',
      name: 'ranking',
      component: Ranking
    },
    {
      path: '/perdiste',
      name: 'perdiste',
      component: Perdiste,
      beforeEnter: (to, from, next) => {
        if (!store.state.isLoggedIn || from.name != 'play' && !store.state.debug)
          next('/')
        else
          next()
      }

    }
  ]
})

router.beforeEach((from, to, next) => {
  // matar countdown si existe
  if (typeof window.countdown !== 'undefined')
    clearInterval(window.countdown)

  // descargar palabras si no se han descargado
	if (store.state.words.length == 0) {
    	axios
    	.get(store.state.api.words)
    	.then((data) => {
      		store.commit('setWords', data.data.data)
    	})
	}
	next()
})

export default router
