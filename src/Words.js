export default [
	{
		id: 1,
		word: 'Limpiar'
	},
	{
		id: 2,
		word: 'Secar'
	},
	{
		id: 3,
		word: 'Cocinar'
	},
	{
		id: 4,
		word: 'Absorber'
	},
	{
		id: 5,
		word: 'Rendir'
	},
	{
		id: 6,
		word: 'Trapear'
	},
	{
		id: 7,
		word: 'Abrillantar'
	},
	{
		id: 8,
		word: 'Arreglar'
	},
	{
		id: 9,
		word: 'Jugar'
	},
	{
		id: 10,
		word: 'Crear'
	},
	{
		id: 11,
		word: 'Cuidar'
	},
	{
		id: 12,
		word: 'Salvar'
	},
	{
		id: 13,
		word: 'Enfriar'
	},
	{
		id: 14,
		word: 'Higienizar'
	},
	{
		id: 15,
		word: 'Hacer aviones'
	},
	{
		id: 16,
		word: 'Hacer barquitos'
	},
	{
		id: 17,
		word: 'Hacer flores'
	},
	{
		id: 18,
		word: 'Guardar uvas'
	},
	{
		id: 19,
		word: 'Guardar frutillas'
	},
	{
		id: 20,
		word: 'Dibujar y pintar'
	},
	{
		id: 21,
		word: 'Guardar frutas'
	},
	{
		id: 22,
		word: 'Envolver el sandwich'
	},
	{
		id: 23,
		word: 'Equilibrar la mesa'
	},
	{
		id: 24,
		word: 'Limpiar espejos'
	},
	{
		id: 25,
		word: 'Limpiar zapatos'
	},
	{
		id: 26,
		word: 'Limpiar TV'
	},
	{
		id: 27,
		word: 'Limpiar plantas'
	},
	{
		id: 28,
		word: 'limpiar pizarra'
	},
	{
		id: 29,
		word: 'Limpiar la nariz'
	},
	{
		id: 30,
		word: 'Sacar brillo a las plantas'
	},
	{
		id: 31,
		word: 'Sacar el polvo'
	},
	{
		id: 32,
		word: 'Calentar fajitas'
	},
	{
		id: 33,
		word: 'Absorber aceite '
	},
	{
		id: 34,
		word: 'usarlo de babero'
	},
	{
		id: 35,
		word: 'Tomar cosas calientes'
	},
	{
		id: 36,
		word: 'Limpiar papeles'
	},
	{
		id: 38,
		word: 'Tomar apuntes'
	},
	{
		id: 39,
		word: 'Secar las manos'
	},
	{
		id: 40,
		word: 'Abrir una bebida'
	},
	{
		id: 41,
		word: 'Prender la parrilla'
	},
	{
		id: 42,
		word: 'recoger vidrios del suelo'
	},
	{
		id: 43,
		word: 'Filtrar el cafe'
	},
	{
		id: 44,
		word: 'Secar tablas de madera'
	},
	{
		id: 45,
		word: 'Limpiar hilos del choclo'
	},
	{
		id: 46,
		word: 'Filtrar la grasa de las sopas'
	},
	{
		id: 47,
		word: 'Mantener las verduras frescas'
	},
	{
		id: 48,
		word: 'Secar la humedad'
	},
	{
		id: 49,
		word: 'Tope de puerta'
	},
	{
		id: 50,
		word: 'Quitar las manchas'
	},
	{
		id: 51,
		word: 'TODO'
	}
]