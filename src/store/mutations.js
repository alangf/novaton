export default {
	setUserInfo (state, data) {
		state.user_id = data.user_id
		state.facebook_id = data.facebook_id
		state.name = data.name
		state.email = data.email
		state.facebook_access_token = data.facebook_access_token
	},
	setUserPicture (state, picture) {
		state.picture_url = picture
	},
	setUserId (state, userId) {
		state.user_id = userId
	},
	login (state) {
		state.isLoggedIn = true
	},
	logout (state) {
		state.facebook_id = false
		state.name = false
		state.email = false
		state.picture_url = false
		state.facebook_access_token = false
		state.isLoggedIn = false
	},
	changeColor (state, color) {
		state.currentColor = color
	},
	updateTotal (state, total) {
		state.total = total
	},
	updateScore (state, score) {
		state.score = score
	},
	addScore (state, score) {
		state.score += score
	},
	setLoginStatus (state, value) {
		state.isLoggedIn = value
	},
	setScore (state, score) {
		state.score = score
	},
	setWords (state, words) {
		state.words = words
	}
}