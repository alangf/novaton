export default {
	api: {
		'words': 'http://novaton.multinetlabs.com/api/word/list',
		'ranking': 'http://novaton.multinetlabs.com/api/att/ranking',
		'auth': 'http://novaton.multinetlabs.com/api/user',
		'attempt': 'http://novaton.multinetlabs.com/api/att'
	},
	colors: ['turq', 'purple', 'pink', 'blue', 'light-pink'],
	cuadros: [
		{
			name: 'Experimento nº 0',
			author: 'Diego Solari',
			link: 'https://www.teleton.cl/galeria-de-arte/experimento-no-0-2', 
			minScore: 1000,
			imgName: 'cuadro_01.png'
		},		
		{
			name: 'Experimento nº 26',
			author: 'Maritza Zamora',
			link: 'https://www.teleton.cl/galeria-de-arte/experimento-no-26/', 
			minScore: 2000,
			imgName: 'cuadro_02.png'
		},
		{
			name: 'Experimento Nº 0000001',
			author: 'Tomás Donoso',
			link: 'https://www.teleton.cl/galeria-de-arte/experimento-no-0000001/', 
			minScore: 4000,
			imgName: 'cuadro_03.png'
		},
		{
			name: 'Experimento Nº 714',
			author: 'Paula Abarca',
			link: 'https://www.teleton.cl/galeria-de-arte/experimento-no-714/', 
			minScore: 7000,
			imgName: 'cuadro_04.png'
		},
		{
			name: 'Las Flores',
			author: 'Paula Abarca',
			link: 'https://www.teleton.cl/galeria-de-arte/las-flores/', 
			minScore: 10000,
			imgName: 'cuadro_05.png'
		},
		{
			name: '',
			author: 'Catalína Farías',
			link: '', 
			minScore: 15000,
			imgName: ''
		},
		{
			name: 'Infraestructura',
			author: 'Luis Felipe Sáez',
			link: 'https://www.teleton.cl/galeria-de-arte/infraestructura/', 
			minScore: 20000,
			imgName: 'cuadro_07.jpg'
		},
		{
			name: 'Experimento Nº 7',
			author: 'Felipe Pallares',
			link: 'https://www.teleton.cl/galeria-de-arte/experimento-no-7/', 
			minScore: 30000,
			imgName: 'cuadro_08.png'
		},
		{
			name: 'Experimento nº 26',
			author: 'Maritza Zamora',
			link: 'https://www.teleton.cl/galeria-de-arte/experimento-no-26/', 
			minScore: 40000,
			imgName: 'cuadro_09.png'
		},
		{
			name: '',
			author: 'Gabriel Gajardo',
			link: '', 
			minScore: 50000,
			imgName: ''
		}
	],
	currentColor: 0,
	debug: false,
	/*
	defaultUser: {
		user_id: 15,
		email: 'alangf@gmail.com',
		facebook_id: 10155619214005049,
		picture_url: 'https://scontent.xx.fbcdn.net/v/t1.0-1/p320x320/14681811_10154413209150049_3760988988776973291_n.jpg?oh=532c229a36869141ebcdb57f91848afb&oe=5A6D0B74'
	},*/
	email: false,
	facebook_access_token: '',
	facebook_id: 0,
	name: '',
	gameOverOnTimeout: true,
	isLoggedIn: false,
	picture_url: false,
	score: 0,
	total: 0,
	user_id: false,
	words: []
}