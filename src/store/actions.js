import axios from 'axios'
import store from '../store'

export default {
	verifyUser (context, user) {
		axios
		.post(context.state.api.auth, user)
		.then((response) => {
			if (context.state.debug)
				console.log('user', response)
			if (response.data.data.length > 0) {
				if (context.state.debug) 
					console.log('setUserId', response.data.data[0].id)
				context.commit('setUserId', response.data.data[0].id)
				let total = parseInt(typeof response.data.total === 'number' ? response.data.total : response.data.total[0].total)
				if (context.state.debug)
					console.log('updateTotal', total)
				context.commit('updateTotal', total)
			} else if (typeof response.data.data.id !== undefined) {
				if (context.state.debug) 
					console.log('setUserId', response.data.data.id)
				context.commit('setUserId', response.data.data.id)
				let total = parseInt(typeof response.data.total === 'number' ? response.data.total : response.data.total[0].total)
				if (context.state.debug)
					console.log('updateTotal', total)
				context.commit('updateTotal', total)
			}
		})		
	},
	saveAttempt (context, data) {
		if (context.state.debug) {
			context.commit('updateTotal', context.state.total + data.punto)
		} else {
			axios
			.post(context.state.api.attempt, data)
			.then((response) => {
				if (context.state.debug)
					console.log('saveAttempt', response.data.data)
				let total = typeof response.data.data === 'Object' ? response.data.data.total : response.data.data[0].total
				if (context.state.debug)
					console.log('total', total)
				context.commit('updateTotal', parseInt(total))
			})			
		}
	}
}